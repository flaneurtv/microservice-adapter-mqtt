package main

import (
	"time"
	"strconv"
	"sync"
	"fmt"
	"log"
	"os"
	"bufio"
)

func main() {
  scanner := bufio.NewScanner(os.Stdin)

	iterations, _ := strconv.Atoi(os.Getenv("COUNT"))

	var wg sync.WaitGroup
	wg.Add(1)

	go func() {
		counter := 0
		for scanner.Scan() {
			msg := scanner.Text()
			counter += 1
			fmt.Fprintf(os.Stderr, "Received %d: %s\n", counter, msg)
			if counter == iterations {
				break
			}
		}
		if err := scanner.Err(); err != nil {
			log.Println(err)
		}
		wg.Done()
	}()

	start := time.Now()

	message := `{"topic":"default/test/benchmark","payload":`
	for i := 0; i < iterations; i++ {
		fmt.Printf("%s%d}\n", message, i+1)
		//wg.Done()
	}

	wg.Wait()
	stop := time.Now()
	duration := stop.Sub(start)

  m := `{"topic":"default/test/benchmark_result","payload":{"iterations":%d, "duration": "%s", "ops_per_sec": "%f", "duration_per_op":"%s"}}`
  	fmt.Printf(m + "\n", iterations, duration, float64(iterations) / duration.Seconds(), time.Duration(duration.Nanoseconds() / int64(iterations)))
}

// FIXME: Ass messages are received back on stdin, but half the time up to 60
// log messages corresponding to these lines are not print! I suspect that being
// a problem with samm, as I can not reproduce this when using an io.Pipe.
// Run this code like below, does not show the same problems:
// mkfifo fifo
// COUNT=10000 /srv/processor < fifo | cat > fifo
